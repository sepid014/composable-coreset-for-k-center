#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <algorithm>
#include <ctime>
#include <cmath>
using namespace std;

#define N 10000
#define D 50
#define K N
#define Type double
#define IND int

int d; // # dimensions


//random number generator
IND random(IND n)
{
	return (n*rand()/RAND_MAX);
}
//distance function
Type dist(vector<Type> a, vector<Type> b)
{
	Type sum = 0;
	for(int i = 0; i < a.size(); i++)
	{
		sum += (a[i] - b[i])*(a[i] - b[i]);
	}
	return sqrt(sum);
}

int main(int argc, char *argv[])
{
	int temp=0;
	IND n; // # points
	int MK;
	Type r; // # clusters
/* 	//read n,d
	stringstream ssin;
	// argv[0] is the file path
	ssin.str(argv[1]);
	ssin >> n;
	ssin.clear();
	ssin.str(argv[2]);
	ssin >> d;
	ssin.clear();
	ssin.str(argv[3]);
	ssin >> r; */
	d=26;
	n=5875;
	//read input points: all points to the first cluster
	vector<Type> point(d);
	vector< vector<Type> > cluster;
	for(IND i = 0; i < n; i++ )
	{
		for(int j = 0; j < d; j++)
		{
			cin >> point[j];
		}
		cluster.push_back(point);
	}
	for(double r = 15; r<=45; r+=1)
	{
	MK=n;
	//srand
	for(int t=0; t < 100; t++)
	{
	srand(t);
	vector< vector< vector<Type> > > clusters(K); //clusters[K][N][D]
	
	clusters[0] = cluster;
	
	//run greedy dual clustering [+Richness]
	vector< vector<Type> > centers(K);
	vector< double > radius(K,0);
	vector< IND > farthest(K,0);
	
	//inti: 1-cluster, a random point is the center
	int randy=random(n);
	//copy center
	centers[0]=clusters[0][randy];
	clusters[0].erase(clusters[0].begin() + randy);
	//compute farthest
	for(IND i = 0; i < clusters[0].size(); i++)
	{
		if(dist(centers[0],clusters[0][i])>radius[0])
		{
			farthest[0]=i;
			radius[0]=dist(centers[0],clusters[0][i]);
		}
		else if (dist(centers[0], clusters[0][i]) == radius[0])
		{
			if (random(2))
			{
				farthest[0] = i;
				//the radius is the same
			}
		}
	}
	// current farthest neighbor
	int k = 1;
	int cur = 0;
	while(radius[cur] > r)
	{
		//add a new cluster
		//assign farthest[cur] as the new cluster's center
		centers[k]=clusters[cur][farthest[cur]];
		clusters[cur].erase(clusters[cur].begin()+farthest[cur]);
		//split the cur cluster (re-cluster)
		vector< vector<Type> > points(clusters[cur]);
		clusters[cur].erase(clusters[cur].begin(),clusters[cur].end());
		radius[cur] = radius[k] = 0;
		for(IND i = 0; i < points.size(); i++ )
		{
			if( dist(centers[k],points[i])>dist(centers[cur],points[i]) )
			{
				// add to cluster cur
				clusters[cur].push_back(points[i]);
				if (radius[cur] < dist(centers[cur], points[i]))
				{
					// update radius of cur
					radius[cur] = dist(centers[cur], points[i]);
					// update farthest
					farthest[cur] = clusters[cur].size() - 1;
				}
				else if (radius[cur] == dist(centers[cur], points[i]))
				{
					//random replacement (for richness)
					if (random(2))
						farthest[cur] = clusters[cur].size() - 1;
				}
			}
			else if (dist(centers[k], points[i])<dist(centers[cur], points[i]))
			{
				// add to cluster num
				clusters[k].push_back(points[i]);
				if (radius[k] < dist(centers[k], points[i]))
				{
					// update radius of num
					radius[k] = dist(centers[k], points[i]);
					// update farthest
					farthest[k] = clusters[k].size() - 1;
				}
				else if (radius[k] == dist(centers[k], points[i]))
				{
					//random replacement (for richness)
					if (random(2))
						farthest[k] = clusters[k].size() - 1;
				}
			}
			else //equal distance
			{
				//randomly assign to a cluster
				if (random(2))
				{
					//first
					clusters[cur].push_back(points[i]);
					//random replacement (for richness)
					if (random(2))
						farthest[cur] = clusters[cur].size() - 1;
				}
				else
				{
					//second
					clusters[k].push_back(points[i]);
					//random replacement (for richness)
					if (random(2))
						farthest[k] = clusters[k].size() - 1;
				}
			}

		}//end for

		// update cluster count
		k++;

		//find the maximum radius cluster
		cur = 0;
		for (int i = 1; i < k; i++)
		{
			//This also should be random!
			if (radius[i] > radius[cur])
			{
				cur = i;
			}
			else if (radius[i] == radius[cur])
			{
				if (random(2))
					cur = i;
			}
		}
		points.erase(points.begin(),points.end());
		//cerr << radius[cur] << " " << k << endl;
	}//end while
	if(k <= MK)
	{
		temp=t;
		MK=k;
	}
	}
	//copy!
	srand(temp);
	vector< vector< vector<Type> > > clusters(K); //clusters[K][N][D]
	//read input points: all points to the first cluster
	clusters[0] = cluster;
	
	//run greedy dual clustering [+Richness]
	vector< vector<Type> > centers(K);
	vector< double > radius(K,0);
	vector< IND > farthest(K,0);
	
	//inti: 1-cluster, a random point is the center
	int randy=random(n);
	//copy center
	centers[0]=clusters[0][randy];
	clusters[0].erase(clusters[0].begin() + randy);
	//compute farthest
	for(IND i = 0; i < cluster[0].size(); i++)
	{
		if(dist(centers[0],clusters[0][i])>radius[0])
		{
			farthest[0]=i;
			radius[0]=dist(centers[0],clusters[0][i]);
		}
		else if (dist(centers[0], clusters[0][i]) == radius[0])
		{
			if (random(2))
			{
				farthest[0] = i;
				//the radius is the same
			}
		}
	}
	// current farthest neighbor
	int k = 1;
	int cur = 0;
	while(radius[cur] > r)
	{
		//add a new cluster
		//assign farthest[cur] as the new cluster's center
		centers[k]=clusters[cur][farthest[cur]];
		clusters[cur].erase(clusters[cur].begin()+farthest[cur]);
		//split the cur cluster (re-cluster)
		vector< vector<Type> > points(clusters[cur]);
		clusters[cur].erase(clusters[cur].begin(),clusters[cur].end());
		radius[cur] = radius[k] = 0;
		for(IND i = 0; i < points.size(); i++ )
		{
			if( dist(centers[k],points[i])>dist(centers[cur],points[i]) )
			{
				// add to cluster cur
				clusters[cur].push_back(points[i]);
				if (radius[cur] < dist(centers[cur], points[i]))
				{
					// update radius of cur
					radius[cur] = dist(centers[cur], points[i]);
					// update farthest
					farthest[cur] = clusters[cur].size() - 1;
				}
				else if (radius[cur] == dist(centers[cur], points[i]))
				{
					//random replacement (for richness)
					if (random(2))
						farthest[cur] = clusters[cur].size() - 1;
				}
			}
			else if (dist(centers[k], points[i])<dist(centers[cur], points[i]))
			{
				// add to cluster num
				clusters[k].push_back(points[i]);
				if (radius[k] < dist(centers[k], points[i]))
				{
					// update radius of num
					radius[k] = dist(centers[k], points[i]);
					// update farthest
					farthest[k] = clusters[k].size() - 1;
				}
				else if (radius[k] == dist(centers[k], points[i]))
				{
					//random replacement (for richness)
					if (random(2))
						farthest[k] = clusters[k].size() - 1;
				}
			}
			else //equal distance
			{
				//randomly assign to a cluster
				if (random(2))
				{
					//first
					clusters[cur].push_back(points[i]);
					//random replacement (for richness)
					if (random(2))
						farthest[cur] = clusters[cur].size() - 1;
				}
				else
				{
					//second
					clusters[k].push_back(points[i]);
					//random replacement (for richness)
					if (random(2))
						farthest[k] = clusters[k].size() - 1;
				}
			}

		}//end for

		// update cluster count
		k++;

		//find the maximum radius cluster
		cur = 0;
		for (int i = 1; i < k; i++)
		{
			//This also should be random!
			if (radius[i] > radius[cur])
			{
				cur = i;
			}
			else if (radius[i] == radius[cur])
			{
				if (random(2))
					cur = i;
			}
		}
		points.erase(points.begin(),points.end());
		//cerr << radius[cur] << " " << k << endl;
	}//end while
	
	//output the size of the core-set
	cerr<< k << endl;
	//output radius
	cerr << radius[cur] << endl;
	//output cluster centers and their counts
	for(int i = 0; i < k; i++ )
	{
		for(int j = 0; j < d; j++)
			cout << centers[i][j] << "\t";
		cout << clusters[i].size() << endl;
	}
	}
	return 0;
}
