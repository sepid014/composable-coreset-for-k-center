#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <algorithm>
#include <ctime>
using namespace std;

#define N 10000
#define D 50
#define K (ceil(sqrt(1.0*N)))
#define Type float
#define IND int

int d; // # dimensions


//random number generator
IND random(IND n)
{
	return rand()%n;
}
//distance function
Type dist(vector<Type> a, vector<Type> b)
{
	Type sum = 0;
	for(int i = 0; i < a.size(); i++)
	{
		sum += (a[i] - b[i])*(a[i] - b[i]);
	}
	return sqrt(sum);
}

int main(int argc, char *argv[])
{
	srand(time(0));
	IND n; // # points
	Type r; // # clusters
	vector< vector<Type> > points; //input points[N][D]
	//read n,d
	stringstream ssin(argv[1]);
	ssin >> n;
	ssin.clear();
	ssin.str(argv[2]);
	ssin >> d;
	ssin.clear();
	ssin.str(argv[3]);
	ssin >> r;
	//read input points: all points to the first cluster
	vector<Type> point(d);
	for(IND i = 0; i < n; i++ )
	{
		for(int j = 0; j < d; j++)
		{
			cin >> point[j];
		}
		points.push_back(point);
	}
	//build the graph
	vector<IND> adj[N];
	for(IND i = 0; i < n; i++ )
	{
		for(IND j = i+1; j < n; j++ )
		{
			if( dist(points[i],points[j]) <= r )
			{
				adj[i].push_back(j);
				adj[j].push_back(i);
			}
		}
	}
	//build the square of the graph
	vector<IND> adj2[N];
	for(IND i = 0;i < N; i++ )
	{
		for(IND j=0; j < adj[i].size(); j++ )
		{
			for(IND k = 0; k < adj[j].size(); k++)
			{
				adj2[i].push_back(adj[adj[i][j]][k]);
			}
		}
	}
	//clusters
	vector< vector<Type> > centers(K);
	vector<IND> cnt(K,1);
	int k=0; //size of the core-set
	//compute an independent set
	vector<IND> perm(n);
	for(IND i = 0; i < n; i++ )
		perm[i]=i;
	random_shuffle(perm.begin(),perm.end());
	vector<int> mark(n,0);//is this point in IS: 0 (unvisited), 1(in IS), 2 (neighbor in IS)
	for(IND t = 0; t < n; t++ )
	{
		IND i = perm[t];
		if(mark[i] == 0 )
		{
			//add to independent set
			mark[i] = 1;
			centers[k]=points[i];
			//remove neighbors
			for(IND j = 0; j < adj[i].size(); j++ )
			{
				if(mark[adj[i][j]]==0 )
				{
					mark[adj[i][j]] = 2;
					cnt[k]++;
				}
			}
			k++;
		}
	}

	//output the size of the core-set
	cout << k << endl;
	//output cluster centers and their counts
	for(int i = 0; i < k; i++ )
	{
		for(int j = 0; j < d; j++)
			cout << centers[i][j] << "\t";
		cout << cnt[i] << endl;
	}
	
	return 0;
}
